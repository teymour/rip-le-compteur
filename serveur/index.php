<!DOCTYPE html>

<html lang="fr">

<head>
    <meta charset="utf-8" />
    <title>RIP, le compteur</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#0070C0">
    <link rel="stylesheet" href="https://dav.li/forkawesome/1.0.11/css/fork-awesome.min.css" />
    <style>
        body {
            font-family: sans-serif;
            margin: 0;
        }

        header {
            text-align: center;
            padding: 50px 0;
        }

        h1,h2,p {
            margin: 20px auto;
            width: 80%;
        }

        h1 {
            font-size: 5em;
        }

        section {
            padding: 20px;
            background-color: #f0f0f0;
        }
        section.centered{
            text-align: center;
            background-color: white;
        }

        footer {
            bottom: 0;
            right: 0;
            left: 0;
            padding: 20px;
            background-color: #3c3c3c;
            color: white;
        }

        a {
            color: inherit;
            font-weight: bold;
            text-decoration: none;
        }

        a:hover {
            text-decoration: underline;
        }
        
        .btn{
            background-color: #f0f0f0;
            border-radius: 5px;
            padding: 10px;
            border:2px solid #f0f0f0;
        }
        .btn:hover{
            text-decoration: none;
            border-color: #3c3c3c;
        }
        .btn:active{
            opacity: 0.5;
        }
        
        .stats{
            display: block;
            overflow: auto;
        }
        .stats th {
            text-align: left;
        }
        .stats .bar{
            display: inline-block;
            background-color: #9d9d9d;
        }
        
    </style>
</head>

<body>
    <header>
        <?php
                $counter=[];
                ini_set('auto_detect_line_endings',TRUE);
                $handle = fopen("data.txt",'r');
                while ( ($data = fgetcsv($handle) ) !== FALSE ) {
                    if($data[0]!=null){
                        $counter[$data[3]]=$data[4];
                    }
                }
                ini_set('auto_detect_line_endings',FALSE);
                $total=0;
                $withPagination=0;
                foreach($counter as $c){
                    $total+=$c;
                    if($c%100==1){
                        $withPagination++;
                    }
                }
            ?>
            <h1>
                <?php echo($total); ?>
            </h1>
            <p>Signatures détectées sur la <a href="https://www.referendum.interieur.gouv.fr/" target="_blank">proposition de loi référendaire</a> visant à affirmer le caractère de service public national de l'exploitation des aérodromes de Paris (avec une marge d'erreur de plus ou moins <?php echo($withPagination*100); ?> dû au mode de calcul : le chiffre exact se situe donc entre <?php echo($total-$withPagination*100); ?> et <?php echo($total+$withPagination*100); ?>).</p>
            <p><i>Attention : étant donné que le comptage est collaboratif, des erreurs peuvent altérer la véracité de ce chiffre. Toutes les données sont <a href="data.txt" target="_blank">en libre accès</a> pour faire vos propres calculs et vérifications.</i></p>
    </header>
    <main>
        <section>
            <h2>Aider à améliorer cette statistique en installant un module sur votre navigateur</h2>
            <p>Le site du ministère de l'intérieur ne permet pas d'obtenir le nombre de signatures total. Pour avoir ce chiffre, il faudrait que chacun compte les signatures affichées sur le site web. Heureusement, un script permet de le faire à votre place et de transmettre ce résultat à ce site web. L'objectif est de centraliser le comptage fait par les citoyens qui ont installé l'extension sur leur navigateur et ainsi d'avoir le nombre de signataires.</p>
            <p>Après avoir installé l'extension, il vous faudra simplement visiter des pages tel que https://www.referendum.interieur.gouv.fr/consultation_publique/8/A/AA et résoudre les "captcha". L'extension détectera automatiquement les signatures. Merci !</p>
        </section>
        <section class="centered">
            <p><a href="rip_le_compteur-1.2-fx.xpi" class="btn"><i class="fa fa-download" aria-hidden="true"></i> Installer l'extension sur Firefox</a></p>
        </section>
        <section>
            <h2>Avertissement et conditions d'utilisation</h2>
            <p>L'auteur de ce site web et de l'extension ne peut être tenu responsable d'une mauvaise utilisation ou d'une utilisation malicieuse des outils ou données délivrés par ce site web.</p>
            <p>Les données de ce site web sont en libre accès (<a href="data.txt" target="_blank">base de données compteur</a>, <a href="error.txt" target="_blank">base de données erreur</a>, <a href="https://framagit.org/DavidLibeau/rip-le-compteur" target="_blank">code source</a>) et sont soumis à leur licence respective.</p>
            <p>L'auteur de ce site web et de l'extension ne fournit aucune garantie quant à la validité des données fournies par ce site web.</p>
            <p>En utilisant ce site web et en téléchargement l'extension pour navigateur, les utilisateurs donnent leur approbation concernant ces conditions d'utilsation et leur consentement à l'utilisation et la transmission des données necessaire au bon fonctionnement de ce site web et de l'extension pour navigateur.</p>
            <p>Ce site web et l'extension de navigateur peuvent enregistrer les données de connexion de l'utilisateur et notament son adresse IP. Ce site web utilise des cookies necessaire à son fonctionnement. Aucune données n'est vendue. Certaines données sont en libre accès (<a href="data.txt" target="_blank">base de données compteur</a>, <a href="error.txt" target="_blank">base de données erreur</a>). L'adresse IP de l'utilisateur est anonymisée (pseudonymisation avec un algorithme md5 avec ajout d'une chaine de caractères secrete) avant d'être sauvegardée.</p>
            <p>Avant d'utiliser l'extension de navigateur, l'utilisateur doit s'informer de son fonctionnement en consultant le code source en libre accès (<a href="https://framagit.org/DavidLibeau/rip-le-compteur" target="_blank">à cette adresse</a>) ou en demandant des informations à l'auteur de l'extension, si l'utilisateur a des questions.</p>
            <p>L'utilisateur peut à tout moment exercer ses droits auprès de l'auteur du site web et de l'extension de navigateur. L'auteur du site web et de l'extension de navigateur aura 30 jours pour répondre et exercer les droits de l'utilisateur.</p>
            <p>L'auteur de ce site web et de l'extension de navigateur peut être contacté <a href="https://davidlibeau.fr/Contact" target="_blank">à cette adresse</a>.</p>
        </section>
        <section style="background-color:white">
            <h2>Statistiques supplémentaires</h2>
            <p>
            <?php
                $alphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                /*for ($i=0; $i<26; $i++) {
                    for ($j=0; $j<26; $j++) {
                        if($counter[$alphabet[$i].$alphabet[$j]]==null){
                            echo('<a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$alphabet[$i].'/'.$alphabet[$i].$alphabet[$j].'" target="_blank">'.$alphabet[$i].$alphabet[$j].'</a> / ');
                        }
                    }
                }*/
            ?>
            </p>
            <p>
            <?php echo(sizeof($counter)." couples de lettres dans la base de données. ".$withPagination." couples de lettres avec plus de 200 signatures (donc plusieurs pages)."); ?>
            <table class="stats">
            <?php
                for ($i=0; $i<26; $i++) {
                    for ($j=0; $j<26; $j++) {
                        echo('<tr><th><a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$alphabet[$i].'/'.$alphabet[$i].$alphabet[$j].'" target="_blank">'.$alphabet[$i].$alphabet[$j].'</a></th><th class="bar" style="width:'.($counter[$alphabet[$i].$alphabet[$j]]/10).'px">'.$counter[$alphabet[$i].$alphabet[$j]].'</th></tr>');
                    }
                }
            ?>
            </table>
            </p>
        </section>
    </main>
    <footer>
        <p>CC-BY David Libeau (<a href="https://framagit.org/DavidLibeau/rip-le-compteur" target="_blank">code source</a>)</p>
    </footer>
</body>

</html>